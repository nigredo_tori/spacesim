﻿using UnityEngine;
using System.Collections;
using SpaceSim.Util;
using System;

namespace SpaceSim.Effects
{
    /// <summary>
    /// Реализует анимацию столкновений пуль с поверхностями
    /// </summary>
    [RequireComponent(typeof(ParticleSystem))]
    public class BulletExplosions : MonoBehaviour
    {
        #region Задаваемые снаружи свойства

        /// <summary>
        /// Пул для объектов взрывов
        /// </summary>
        public IPool<Explosion> ExplosionPool { get; set; }

        #endregion

        /// <summary>
        /// Вспомогательный массив
        /// </summary>
        private ParticleCollisionEvent[] collisionEvents;

        new private ParticleSystem particleSystem;

        #region Обработка событий Unity

        public void Awake()
        {
            particleSystem = GetComponent<ParticleSystem>();
            collisionEvents = new ParticleCollisionEvent[8];
        }

        public void OnParticleCollision(GameObject other)
        {
            // Копируем события столкновений в массив
            int safeLength = particleSystem.GetSafeCollisionEventSize();
            if (collisionEvents.Length < safeLength)
            {
                collisionEvents = new ParticleCollisionEvent[safeLength];
            }

            int numCollisionEvents = particleSystem.GetCollisionEvents(other, collisionEvents);

            // Для каждого столкновения создаём взрыв.
            for (int i = 0; i < numCollisionEvents; i++)
            {
                ParticleCollisionEvent collision = collisionEvents[i];
                Explosion explosion = ExplosionPool.Aquire();

                explosion.transform.position = collision.intersection;

                // Z-ось взрыва должна совпадать с нормалью к поверхности
                explosion.transform.rotation = Quaternion.FromToRotation(Vector3.forward, collision.normal);

                // После окончания анимации взрыв возвращается в пул
                explosion.StartCoroutine(ReleaseExplosion(explosion, ExplosionPool));
            }
        }

        #endregion

        #region Корутины

        /// <summary>
        /// Возвращает объект взрыва в пул для дальнейшего повторного использования.
        /// </summary>
        /// <param name="explosion"></param>
        /// <param name="explosionPool"></param>
        /// <returns></returns>
        private static IEnumerator ReleaseExplosion(Explosion explosion, IPool<Explosion> explosionPool)
        {
            yield return new WaitForSeconds(explosion.duration);
            explosionPool.Release(explosion);
        }

        #endregion
    }
}
