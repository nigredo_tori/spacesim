﻿using UnityEngine;
using System.Collections;

namespace SpaceSim.Effects
{

    /// <summary>
    /// Спецэффект взрыва. При повторном включении начинается заново.
    /// 
    /// Если отображает взрыв при совпадении с поверхностью - нормаль столкновения совпадает с локальной осью Z
    /// </summary>
    public class Explosion : MonoBehaviour
    {

        [Tooltip("Длительность взрыва в секундах. По истечении этого времени объект можно будет удалить.")]
        public float duration;

        private ParticleSystem[] particleSystems;

        public void Awake()
        {
            particleSystems = GetComponentsInChildren<ParticleSystem>();
        }

        public void OnDisable()
        {
            foreach (var particleSystem in particleSystems)
            {
                particleSystem.Stop();
            }
        }

        public void OnEnable()
        {
            foreach (var particleSystem in particleSystems)
            {
                particleSystem.Play();
            }
        }
    }
}
