﻿using UnityEngine;
using System.Collections;
using SpaceSim.UserInput;

namespace SpaceSim.Effects
{
    /// <summary>
    /// Реализует управление эффектом двигателя корабля в зависимости от заданной тяги
    /// </summary>
    [RequireComponent(typeof(ParticleSystem))]
    public class ThrustJetIntensity : MonoBehaviour
    {

        /// <summary>
        /// Используемый источник ввода
        /// </summary>
        public IInputSource inputSource { get; set; }

        new private ParticleSystem particleSystem;
        private float baseLifetime;

        public void Awake()
        {
            particleSystem = GetComponent<ParticleSystem>();
            baseLifetime = particleSystem.startLifetime;
        }

        public void FixedUpdate()
        {
            float thrust = inputSource.ThrustAxisInput;
            bool engineEnabled = thrust > 0;

            particleSystem.enableEmission = engineEnabled;

            if (engineEnabled)
            {
                particleSystem.startLifetime = thrust * baseLifetime;
            }
        }
    }
}