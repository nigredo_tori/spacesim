﻿using UnityEngine;
using System.Collections;
using SpaceSim.ShipModel;

namespace SpaceSim.Effects
{
    /// <summary>
    /// Отображает анимацию взрыва корабля
    /// </summary>
    public class ShipDeathEffect
    {
        private readonly GameObject shipGameObject;
        private readonly Explosion shipExplosionPrefab;

        public ShipDeathEffect(
            IntegrityModel integrityModel,
            Explosion shipExplosionPrefab,
            GameObject shipGameObject)
        {
            this.shipGameObject = shipGameObject;
            this.shipExplosionPrefab = shipExplosionPrefab;

            integrityModel.ShipDied += IntegrityModel_OnShipDied;
        }

        private void IntegrityModel_OnShipDied(object sender, ShipDiedEventArgs e)
        {
            Vector3 position = shipGameObject.transform.position;
            Explosion explosion = (Explosion) GameObject.Instantiate(shipExplosionPrefab, position, Quaternion.identity);
            GameObject.Destroy(explosion.gameObject, explosion.duration);
        }
    }
}