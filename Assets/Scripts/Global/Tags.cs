﻿using UnityEngine;
using System.Collections;

namespace SpaceSim.Global
{
    /// <summary>
    /// Константы для обращения к слоям
    /// </summary>
    public static class Tags
    {
        public static readonly string PLAYER_SPAWN_POINT = "PlayerSpawnPoint";
        public static readonly string ENEMY_SPAWN_POINT = "EnemySpawnPoint";
    }
}