﻿using UnityEngine;
using System.Collections;

namespace SpaceSim.Global
{
    /// <summary>
    /// Константы для обращения к слоям
    /// </summary>
    public static class Layers
    {
        public static readonly int BULLETS = LayerMask.NameToLayer("Bullets");
        public static readonly int PLAYER_SHIP = LayerMask.NameToLayer("Player Ship");
        public static readonly int ENEMY_SHIP = LayerMask.NameToLayer("Enemy Ship");
        public static readonly int LEVEL_GEOMETRY = LayerMask.NameToLayer("Level Geometry");
    }
}