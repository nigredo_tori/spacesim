﻿using UnityEngine;
using System.Collections;

namespace SpaceSim.Cam
{

    /// <summary>
    /// Постоянно перемещает объект в ту же точку что и цель, с тем же направлением,
    /// до тех пор пока цель не будет уничтожена
    /// </summary>
    public class FollowWhileExists : MonoBehaviour
    {

        [Tooltip("Объект, за которым следует камера")]
        public GameObject target;

        public void Start()
        {
            UpdatePosition();
        }

        void LateUpdate()
        {
            UpdatePosition();
        }

        private void UpdatePosition()
        {
            if (target)
            {
                transform.position = target.transform.position;
                transform.rotation = target.transform.rotation;
            }
        }
    }
}