﻿using UnityEngine;
using System.Collections;
using System;

namespace SpaceSim.Cam
{
    /// <summary>
    /// Переключает камеру между режимами первого и третьего лица при нажатии кнопки "C".
    /// Изначально включает режим от третьего лица
    /// </summary>
    public class CameraSwitcher : MonoBehaviour
    {
        /// <summary>
        /// Режимы камеры
        /// </summary>
        public enum CameraMode
        {
            FirstPerson,
            ThirdPerson
        }

        #region Поля для задания в инспекторе

        [Tooltip("Камера от первого лица")]
        public Camera firstPersonCamera;

        [Tooltip("Камера от третьего лица")]
        public Camera thirdPersonCamera;

        #endregion

        #region Свойство Mode

        private CameraMode _mode;

        public CameraMode Mode
        {
            get
            {
                return _mode;
            }

            private set
            {
                _mode = value;

                firstPersonCamera.gameObject.SetActive(value == CameraMode.FirstPerson);
                thirdPersonCamera.gameObject.SetActive(value == CameraMode.ThirdPerson);
            }
        }

        #endregion

        #region Обработка событий Unity

        public void Start()
        {
            Mode = CameraMode.ThirdPerson;
        }

        public void Update()
        {
            if (UnityEngine.Input.GetKeyDown(KeyCode.C))
            {
                Mode = NextCameraMode(Mode);
            }
        }

        #endregion

        #region Вспомогательные методы

        /// <summary>
        /// Возвращает следующий вариант вида камеры
        /// </summary>
        /// <param name="mode"></param>
        /// <returns></returns>
        private static CameraMode NextCameraMode(CameraMode mode)
        {
            switch (mode)
            {
                case CameraMode.FirstPerson:
                    return CameraMode.ThirdPerson;
                case CameraMode.ThirdPerson:
                    return CameraMode.FirstPerson;
                default:
                    throw new ArgumentException("mode");
            }
        }

        #endregion
    }
}