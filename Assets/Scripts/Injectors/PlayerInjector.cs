﻿using UnityEngine;
using System.Collections;
using SpaceSim.ShipModel;
using SpaceSim.Effects;
using SpaceSim.UserInput;
using SpaceSim.Util;

namespace SpaceSim.Injectors
{
    /// <summary>
    /// Подключает и настраивает компоненты корабля игрока
    /// </summary>
    public class PlayerInjector : ShipInjector
    {
        protected override IInputSource GetInputSource()
        {
            return gameObject.AddComponent<PlayerInputSource>();
        }
    }
}