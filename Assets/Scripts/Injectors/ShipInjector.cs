﻿using UnityEngine;
using System.Collections;
using SpaceSim.Effects;
using SpaceSim.ShipModel;
using SpaceSim.Util;
using SpaceSim.UserInput;

namespace SpaceSim.Injectors
{
    /// <summary>
    /// Подключает и настраивает компоненты корабля
    /// </summary>
    public abstract class ShipInjector : MonoBehaviour
    {

        #region Поля, задаваемые в инспекторе

        [Tooltip("Информация об управляемости корабля")]
        public ShipData shipData;

        [Tooltip("Префаб взрыва при попадании пули в какую-либо поверхность")]
        public Explosion bulletExplosion;

        [Tooltip("Префаб взрыва корабля при гибели")]
        public Explosion shipExplosion;

        #endregion

        #region Абстрактные методы

        protected abstract IInputSource GetInputSource();

        #endregion

        #region Инициализация

        public void Awake()
        {
            // Инициализация дополнительных объектов
            IInputSource inputSource = GetInputSource();
            IPool<Explosion> explosionPool = new PrefabPool<Explosion>(bulletExplosion);

            // Настройка модели управляемости
            var handlingModel = gameObject.AddComponent<ShipHandlingModel>();
            handlingModel.ShipData = shipData;
            handlingModel.InputSource = inputSource;

            // Настройка модели повреждений
            var integrityModel = gameObject.AddComponent<IntegrityModel>();
            integrityModel.ShipData = shipData;

            // Настройка орудий
            GunModel[] guns = GetComponentsInChildren<GunModel>();
            foreach (var gun in guns)
            {
                gun.InputSource = inputSource;
            }

            // Обработка события смерти корабля
            new ShipDeathHandler(integrityModel, gameObject);

            // Настройка эффекта разрыва снарядов
            BulletExplosions[] explosionsEffects = GetComponentsInChildren<BulletExplosions>();
            foreach (var explosionEffect in explosionsEffects)
            {
                explosionEffect.ExplosionPool = explosionPool;
            }

            // Настройка отображения тяги
            ThrustJetIntensity[] jets = GetComponentsInChildren<ThrustJetIntensity>();
            foreach (var jet in jets)
            {
                jet.inputSource = inputSource;
            }

            // Настройка отображения взрыва корабля
            new ShipDeathEffect(integrityModel, shipExplosion, gameObject);
        }

        #endregion
    }

}
