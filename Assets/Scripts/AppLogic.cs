﻿using UnityEngine;
using System.Collections;
using SpaceSim.UI;
using SpaceSim.Game;
using System;
using UnityEngine.EventSystems;

namespace SpaceSim
{
    /// <summary>
    /// Реализует глобальную логику приложения (отображение окон, перезапуск, закрытие)
    /// </summary>
    public class AppLogic : MonoBehaviour
    {
        #region AppState enum

        /// <summary>
        /// Состояния приложения:
        /// Running - идёт игра
        /// Paused - игра стоит на паузе
        /// GameOver - игра окончена (победа или поражение)
        /// </summary>
        private enum AppState
        {
            Running,
            Paused,
            GameOver
        }

        #endregion

        #region Поля для задания в инспекторе

        [Tooltip("Окно паузы")]
        public PauseUI pauseUIPrefab;

        [Tooltip("Окно конца игры")]
        public GameOverUI gameOverUIPrefab;

        [Tooltip("Главный объект игры")]
        public GameLogic gameLogicPrefab;

        [Tooltip("Префаб уровня")]
        public GameObject levelPrefab;

        #endregion

        #region Cвойство State

        private AppState _state;

        /// <summary>
        /// Текущее состояние приложения
        /// </summary>
        private AppState State
        {
            get
            {
                return _state;
            }

            set
            {
                _state = value;

                UpdateCursorLockState();
            }
        }

        #endregion

        /// <summary>
        /// Созданный объект окна паузы
        /// </summary>
        private PauseUI pauseUI;

        #region Обработка событий Unity

        public void Awake()
        {
            /// Создаём EventSystem
            new GameObject("EventSystem",
                typeof(EventSystem),
                typeof(StandaloneInputModule),
                typeof(TouchInputModule));

            State = AppState.Running;

            // Создаём уровень
            Instantiate(levelPrefab);

            // Запускаем игру:
            var gameLogic = Instantiate(gameLogicPrefab);

            // Обрабатываем событие конца игры:
            gameLogic.GameEnded += GameLogic_GameEnded;
        }

        public void Update()
        {
            if (Input.GetKeyDown(KeyCode.Escape))
            {
                switch (State)
                {
                    case AppState.Running:
                        // Идёт игра - показываем паузу
                        ShowPause();
                        break;

                    case AppState.Paused:
                        // Отображается окно паузы - отключаем
                        HidePause();
                        break;

                    case AppState.GameOver:
                        // Отображается окно конца игры - выходим
                        Exit();
                        break;
                }
            }
        }

        public void OnApplicationFocus(bool focus)
        {
            if (focus)
            {
                UpdateCursorLockState();
            }
            else
            {
                Cursor.lockState = CursorLockMode.None;
            }
        }

        #endregion

        #region Методы для переходов между состояниями

        /// <summary>
        /// Перезапускает игру
        /// </summary>
        private void Restart()
        {
            // Так как в этот момент игра может стоять на паузе
            Time.timeScale = 1;
            Application.LoadLevel(Application.loadedLevel);
        }

        /// <summary>
        /// Скрывает окно паузы
        /// </summary>
        private void HidePause()
        {
            //Прячем меню паузы, продолжаем игру
            State = AppState.Running;
            Time.timeScale = 1;
            pauseUI.gameObject.SetActive(false);
        }

        /// <summary>
        /// Отображает окно паузы
        /// </summary>
        private void ShowPause()
        {
            State = AppState.Paused;
            Time.timeScale = 0;

            if (pauseUI)
            {
                // Уже создали меню паузы
                pauseUI.gameObject.SetActive(true);
                return;
            }

            // Инстанциируем окно паузы, раз не сделали этого раньше
            pauseUI = Instantiate(pauseUIPrefab);

            // подписываемся на события
            pauseUI.ResumeClicked += PauseUI_ResumeClicked;
            pauseUI.RestartClicked += PauseUI_RestartClicked;
            pauseUI.ExitClicked += PauseUI_ExitClicked;
        }

        /// <summary>
        /// Показывает меню окончания игры
        /// </summary>
        /// <param name="result"></param>
        private void ShowGameOverUI(GameResult result)
        {
            State = AppState.GameOver;

            GameOverUI gameOverUI = Instantiate(gameOverUIPrefab);
            gameOverUI.Result = result;

            gameOverUI.RestartClicked += GameOverUI_RestartClicked;
            gameOverUI.ExitClicked += GameOverUI_ExitClicked;
        }

        /// <summary>
        /// Закрывает приложение, либо останавливает воспроизведение в редакторе
        /// </summary>
        private void Exit()
        {
#if UNITY_EDITOR
            UnityEditor.EditorApplication.isPlaying = false;
#else
            Application.Quit();
#endif
        }

        #endregion

        #region Обработчики приходящих событий

        private void GameLogic_GameEnded(object sender, GameEndedEventArgs e)
        {
            GameResult result = e.Result;
            ShowGameOverUI(result);
        }

        private void PauseUI_ExitClicked(object sender, EventArgs e)
        {
            Exit();
        }

        private void PauseUI_RestartClicked(object sender, EventArgs e)
        {
            Restart();
        }


        private void PauseUI_ResumeClicked(object sender, EventArgs e)
        {
            HidePause();
        }

        private void GameOverUI_ExitClicked(object sender, EventArgs e)
        {
            Exit();
        }

        private void GameOverUI_RestartClicked(object sender, EventArgs e)
        {
            Restart();
        }

        #endregion

        #region Вспомогательные методы

        /// <summary>
        /// Заблокировать или разблокировать курсор в зависимости от состояния приложения
        /// </summary>
        private void UpdateCursorLockState()
        {
            bool shouldLock = _state == AppState.Running;
            Cursor.lockState = shouldLock
                ? CursorLockMode.Locked
                : CursorLockMode.None;
            Cursor.visible = !shouldLock;
        }

        #endregion
    }
}
