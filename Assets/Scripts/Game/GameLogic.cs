﻿using UnityEngine;
using System.Collections;
using SpaceSim.Global;
using SpaceSim.Cam;
using SpaceSim.ShipModel;
using System;

namespace SpaceSim.Game
{
    /// <summary>
    /// Инициализирует игру и проверяет условия выигрыша
    /// </summary>
    public class GameLogic : MonoBehaviour
    {
        #region Поля для задания в инспекторе

        [Tooltip("Префаб корабля игрока")]
        public GameObject playerPrefab;

        [Tooltip("Префаб корабля врага")]
        public GameObject enemyPrefab;

        [Tooltip("Префаб объекта камеры")]
        public GameObject cameraPrefab;

        #endregion

        public event EventHandler<GameEndedEventArgs> GameEnded;

        /// <summary>
        /// Количество врагов, которые ещё живы
        /// </summary>
        private int enemiesAlive;

        /// <summary>
        /// Закончилась ли игра (чтобы исключить случай когда наступает и победа и поражение)
        /// </summary>
        private bool gameOver;

        public void Awake()
        {
            gameOver = false;

            // Инициализируем корабль игрока
            Transform playerSpawn = GameObject.FindGameObjectWithTag(Tags.PLAYER_SPAWN_POINT)
                .transform;
            GameObject playerGameObject = SpawnAtPosition(playerPrefab, playerSpawn);
            var playerIntegrity = playerGameObject.GetComponent<IntegrityModel>();
            playerIntegrity.ShipDied += PlayerIntegrity_ShipDied;


            // Инициализируем корабли врагов
            GameObject[] enemySpawnObjects = GameObject.FindGameObjectsWithTag(Tags.ENEMY_SPAWN_POINT);
            enemiesAlive = enemySpawnObjects.Length;

            foreach (var enemySpawnObject in enemySpawnObjects)
            {
                Transform enemySpawn = enemySpawnObject.transform;
                GameObject enemyGameObject = SpawnAtPosition(enemyPrefab, enemySpawn);

                var enemyIntegrity = enemyGameObject.GetComponent<IntegrityModel>();
                enemyIntegrity.ShipDied += EnemyIntegrity_ShipDied;
            }

            // Создаём камеру
            var cameraGameObject = SpawnAtPosition(cameraPrefab, playerSpawn);
            FollowWhileExists followScript = cameraGameObject.GetComponent<FollowWhileExists>();
            followScript.target = playerGameObject;
        }

        #region Обработка событий от игровых объектов

        private void EnemyIntegrity_ShipDied(object sender, ShipDiedEventArgs e)
        {
            if (gameOver)
            {
                return;
            }
            // Проверяем, остались ли ещё враги
            --enemiesAlive;
            if (enemiesAlive == 0)
            {
                gameOver = true;

                OnGameEnded(GameResult.Won);
            }
        }

        private void PlayerIntegrity_ShipDied(object sender, ShipDiedEventArgs e)
        {
            if (gameOver)
            {
                return;
            }

            gameOver = true;

            OnGameEnded(GameResult.Lost);
        }

        #endregion

        #region Вспомогательные методы

        private void OnGameEnded(GameResult gameResult)
        {
            // Отправляем событие OnSpaceSimGameEnded всем игровым объектам
            // Это хак для отключения управления в меню конца игры
            BroadcastMessage("OnSpaceSimGameEnded");

            if (GameEnded != null)
            {
                var args = new GameEndedEventArgs(gameResult);
                GameEnded(this, args);
            }
        }

        /// <summary>
        /// Инстанциирует префаб с позицией и ориентацией, совпадающей с заданным Transform
        /// </summary>
        /// <param name="prefab">Инстанциируемый префаб</param>
        /// <param name="point"></param>
        /// <returns></returns>
        private GameObject SpawnAtPosition(GameObject prefab, Transform point)
        {
            Vector3 position = point.transform.position;
            Quaternion rotation = point.transform.rotation;
            GameObject result = (GameObject)Instantiate(prefab, position, rotation);

            //Все создаваемые объекты - внутри GameLogic
            result.transform.parent = transform;
            return result;
        }

        #endregion
    }

    public class GameEndedEventArgs : EventArgs
    {
        private readonly GameResult _result;

        public GameResult Result
        {
            get
            {
                return _result;
            }
        }

        public GameEndedEventArgs(GameResult result)
        {
            _result = result;
        }
    }

    /// <summary>
    /// Результат игры (победа или поражение)
    /// </summary>
    public enum GameResult
    {
        Won, Lost
    }
}