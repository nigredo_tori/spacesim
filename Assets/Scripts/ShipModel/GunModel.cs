﻿using UnityEngine;
using SpaceSim.UserInput;

namespace SpaceSim.ShipModel
{

    /// <summary>
    /// Реализует поведение орудий корабля в зависимости от состояния управления
    /// </summary>
    [RequireComponent(typeof(ParticleSystem))]
    public class GunModel : MonoBehaviour
    {

        /// <summary>
        /// Источник данных об управлении корабля
        /// </summary>
        public IInputSource InputSource { get; set; }

        /// <summary>
        /// Система частиц, изображающая орудие
        /// </summary>
        private ParticleSystem gunParticleSystem;

        void Start()
        {
            gunParticleSystem = GetComponent<ParticleSystem>();
        }

        void FixedUpdate()
        {
            bool shoot = InputSource.TriggerPressed;
            gunParticleSystem.enableEmission = shoot;
        }
    }
}