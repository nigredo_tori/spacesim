﻿using UnityEngine;
using System.Collections;
using System;

using SpaceSim.Global;

namespace SpaceSim.ShipModel
{
    /// <summary>
    /// Реализует логику обработки попаданий, учёта здоровья, смерти.
    /// </summary>
    public class IntegrityModel : MonoBehaviour
    {

        #region Задаваемые извне свойства

        /// <summary>
        /// Данные корабля
        /// </summary>
        public ShipData ShipData { get; set; }

        #endregion

        #region Свойства для получения информации о корабле

        /// <summary>
        /// Текущее здоровье
        /// </summary>
        public int Health { get; private set; }

        /// <summary>
        /// Погиб ли корабль
        /// </summary>
        public bool Dead
        {
            get
            {
                return Health <= 0;
            }
        }

        #endregion

        #region События

        /// <summary>
        /// Вызывается при смерти корабля
        /// </summary>
        public event EventHandler<ShipDiedEventArgs> ShipDied;

        #endregion

        #region Обработка событий Unity

        void Start()
        {
            Health = ShipData.maxHealth;
        }

        public void OnParticleCollision(GameObject other)
        {
            if (!Dead)
            {
                --Health;
                if (Dead)
                {
                    OnShipDied();
                }
            }
        }

        #endregion

        #region Вспомогательные методы

        private void OnShipDied()
        {
            if (ShipDied != null)
            {
                ShipDiedEventArgs args = new ShipDiedEventArgs(this);
                ShipDied(this, args);
            }
        }

        #endregion
    }

    /// <summary>
    /// Данные о событии смерти.
    /// </summary>
    public class ShipDiedEventArgs : EventArgs
    {
        private readonly IntegrityModel integrityModel;

        public IntegrityModel IntegrityModel
        {
            get
            {
                return integrityModel;
            }
        }

        public ShipDiedEventArgs(IntegrityModel integrityModel)
        {
            this.integrityModel = integrityModel;
        }
    }
}