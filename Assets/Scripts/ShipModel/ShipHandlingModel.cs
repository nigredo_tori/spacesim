﻿using UnityEngine;
using System.Collections;
using SpaceSim.UserInput;

namespace SpaceSim.ShipModel {

    /// <summary>
    /// Отвечает за физику управления корабля.
    /// </summary>
    public class ShipHandlingModel : MonoBehaviour {

        #region Задаваемые извне свойства

        /// <summary>
        /// Выходные данные управления (ИИ или пользовательского ввода)
        /// </summary>
        public IInputSource InputSource { get; set; }

        /// <summary>
        /// "Характеристики управляемости корабля"
        /// </summary>
        public ShipData ShipData { get; set; }

        #endregion

        new private Rigidbody rigidbody;

        #region Обработка событий Unity

        void Awake()
        {
            rigidbody = GetComponent<Rigidbody>();
        }

        void FixedUpdate() {

            // Так как возможна различная тяга вперёд и назад
            float thrustInput = InputSource.ThrustAxisInput;
            float maxThrust =
                thrustInput >= 0
                ? ShipData.maxThrust
                : ShipData.maxReverseThrust;

            Vector3 thrustForce = thrustInput * maxThrust * Vector3.forward;

            // Компоненты желаемой угловой скорости
            float yawVelocity = InputSource.YawAxisInput * ShipData.maxYawAngularVelocity;
            float pitchVelocity = InputSource.PitchAxisInput * ShipData.maxPitchAngularVelocity;
            float rollVelocity = InputSource.RollAxisInput * ShipData.maxRollAngularVelocity;

            Vector3 targetAngularVelocity = transform.TransformDirection(pitchVelocity, yawVelocity, rollVelocity);
            
            // Момент силы зависит от разности между желаемой и текущей угловыми скоростями
            Vector3 torque = (targetAngularVelocity - rigidbody.angularVelocity) * ShipData.torqueCoefficient;

            rigidbody.AddRelativeForce(thrustForce, ForceMode.Force);
            rigidbody.AddTorque(torque, ForceMode.Force);
        }

        #endregion
    }
}