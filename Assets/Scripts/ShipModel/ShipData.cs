﻿using UnityEngine;
using SpaceSim.Util;
using System.Collections;

namespace SpaceSim.ShipModel {

    /// <summary>
    /// Содержит данные о корабле
    /// </summary>
    public class ShipData : ScriptableObject {

        [Tooltip("Название корабля")]
        public string shipName;

        [Tooltip("Максимальная сила тяги, Н")]
        public float maxThrust;

        [Tooltip("Максимальная сила обратной тяги, Н")]
        public float maxReverseThrust;

        [Tooltip("Максимальная угловая скорость по оси тангажа, рад/с")]
        public float maxPitchAngularVelocity;

        [Tooltip("Максимальная угловая скорость по оси рыскания, рад/с")]
        public float maxYawAngularVelocity;

        [Tooltip("Максимальная угловая скорость по оси крена, рад/с")]
        public float maxRollAngularVelocity;

        [Tooltip("Отношение момента силы к разнице между требуемой и текущей скоростями, Н * м  * с / рад")]
        public float torqueCoefficient;

        [Tooltip("Количество попаданий, которое может выдержать корабль")]
        public int maxHealth;
    }
}