﻿using UnityEngine;
using UnityEditor;
using System.Collections;
using SpaceSim.Util;

namespace SpaceSim.ShipModel.Editor
{
    /// <summary>
    /// Настройка редактора для поддержки типов данных проекта
    /// </summary>
    public static class ShipModelEditorUtil
    {

        [MenuItem("Assets/Create/Ship Data")]
        public static void CreateShipData()
        {
            ScriptableObjectUtility.CreateAsset<ShipData>();
        }
    }
}