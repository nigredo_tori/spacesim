﻿using UnityEngine;
using System.Collections;
using System;

namespace SpaceSim.ShipModel {
    /// <summary>
    /// Реализует уничтожение корабля при смерти
    /// </summary>
    public class ShipDeathHandler {

        private readonly GameObject shipGameObject;

        public ShipDeathHandler(
            IntegrityModel integrityModel,
            GameObject shipGameObject)
        {
            this.shipGameObject = shipGameObject;

            integrityModel.ShipDied += IntegirtyModel_OnShipDied;
        }

        private void IntegirtyModel_OnShipDied(object sender, ShipDiedEventArgs e)
        {
            GameObject.Destroy(shipGameObject);
        }
    }
}