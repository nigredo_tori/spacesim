﻿using UnityEngine;
using System.Collections;
using System;
using UnityEngine.UI;
using SpaceSim.Game;

namespace SpaceSim.UI
{
    /// <summary>
    /// Предоставляет возможность получать события окна конца игры и его настройки
    /// </summary>
    public class GameOverUI : MonoBehaviour
    {
        #region Константы

        private const string WON_HEADER = "You won!";
        private const string LOST_HEADER = "You lost!";

        private const string WON_TEXT = "Congratulations!";
        private const string LOST_TEXT = "Better luck next time!";

        #endregion

        #region События

        /// <summary>
        /// Нажата кнопка "Начать заново"
        /// </summary>
        public event EventHandler RestartClicked;

        /// <summary>
        /// Нажата кнопка "Выход"
        /// </summary>
        public event EventHandler ExitClicked;

        #endregion

        #region Поля для задания в инспекторе

        [Tooltip("Заголовок окна")]
        public Text headerElement;

        [Tooltip("Основной текст окна")]
        public Text textElement;

        #endregion

        private GameResult _result;

        /// <summary>
        /// Результат игры.
        /// </summary>
        public GameResult Result
        {
            get { return _result; }
            set
            {
                _result = value;

                headerElement.text = GetHeader(value);
                textElement.text = GetText(value);
            }
        }

        #region Методы, привязываемые к кнопкам

        /// <summary>
        /// Вызывается кнопкой "Начать заново"
        /// </summary>
        public void OnRestartClicked()
        {
            if (RestartClicked != null)
            {
                RestartClicked(this, null);
            }
        }

        /// <summary>
        /// Вызывается кнопкой "Выход"
        /// </summary>
        public void OnExitClicked()
        {
            if (ExitClicked != null)
            {
                ExitClicked(this, null);
            }
        }

        #endregion

        #region Вспомогательные методы

        /// <summary>
        /// Получить основной текст окна для заданного результата игры
        /// </summary>
        /// <param name="value"></param>
        /// <returns></returns>
        private string GetText(GameResult value)
        {
            switch (value)
            {
                case GameResult.Won:
                    return WON_TEXT;
                case GameResult.Lost:
                    return LOST_TEXT;
                default:
                    throw new ArgumentException("Unknown GameResult: " + value);
            }
        }

        /// <summary>
        /// Получить текст заголовка окна для заданного результата игры
        /// </summary>
        /// <param name="value"></param>
        /// <returns></returns>
        private string GetHeader(GameResult value)
        {
            switch (value)
            {
                case GameResult.Won:
                    return WON_HEADER;
                case GameResult.Lost:
                    return LOST_HEADER;
                default:
                    throw new ArgumentException("Unknown GameResult: " + value);
            }
        }

        #endregion
    }
}