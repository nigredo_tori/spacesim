﻿using UnityEngine;
using System.Collections;
using System;

namespace SpaceSim.UI
{
    /// <summary>
    /// Предоставляет возможность получать события окна паузы (нажатия кнопок)
    /// </summary>
    public class PauseUI : MonoBehaviour
    {
        #region Cобытия

        /// <summary>
        /// Нажата кнопка "Продолжить"
        /// </summary>
        public event EventHandler ResumeClicked;

        /// <summary>
        /// Нажата кнопка "Начать заново"
        /// </summary>
        public event EventHandler RestartClicked;

        /// <summary>
        /// Нажата кнопка "Выход"
        /// </summary>
        public event EventHandler ExitClicked;

        #endregion
        
        #region Методы, привязываемые к кнопкам

        /// <summary>
        /// Вызывается кнопкой "Продолжить"
        /// </summary>
        void OnResumeClicked()
        {
            if (ResumeClicked != null)
            {
                ResumeClicked(this, null);
            }
        }

        /// <summary>
        /// Вызывается кнопкой "Начать заново"
        /// </summary>
        void OnRestartClicked()
        {
            if (RestartClicked != null)
            {
                RestartClicked(this, null);
            }
        }

        /// <summary>
        /// Вызывается кнопкой "Выход"
        /// </summary>
        void OnExitClicked()
        {
            if (ExitClicked != null)
            {
                ExitClicked(this, null);
            }
        }

        #endregion
    }
}