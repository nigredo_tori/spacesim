﻿using UnityEngine;
using System.Collections.Generic;
using System;

namespace SpaceSim.Util
{
    /// <summary>
    /// Пул компонентов
    /// 
    /// Необходимые компоненты инстанциируются из переданного префаба.
    /// </summary>
    /// <typeparam name="T"></typeparam>
    public class PrefabPool<T> : IPool<T>
        where T : Component
    {
        #region Объявления полей

        private readonly T prefab;

        private readonly int maxCapacity;
        private readonly Stack<T> stack;

        #endregion

        #region Инициализация
        

        /// <summary>
        /// </summary>
        /// <param name="prefab">Префаб, используемый для создания объектов</param>
        /// <param name="maxCapacity">Максимальная вместимость пула.
        /// Возвращённые сверх этого количества объекты будут уничтожены.
        /// -1 для безразмерного пула.
        /// </param>
        /// <exception cref="NullReferenceException">если префаб - null</exception>
        /// <exception cref="ArgumentException">если maxCapacity < -1</exception>
        public PrefabPool(T prefab, int maxCapacity)
        {
            if (maxCapacity < -1)
            {
                throw new ArgumentException("maxCapacity");
            }

            if (prefab == null)
            {
                throw new NullReferenceException("prefab");
            }

            this.maxCapacity = maxCapacity;
            this.prefab = prefab;
            this.stack = new Stack<T>();
        }

        public PrefabPool(T prefab) : this(prefab, -1)
        {
        }

        #endregion

        #region Реализация IPool

        public T Aquire()
        {
            T result = stack.Count > 0
                ? stack.Pop()
                : GameObject.Instantiate(prefab);

            result.gameObject.SetActive(true);
            return result;
        }

        public void Release(T item)
        {
            item.gameObject.SetActive(false);

            if (maxCapacity == -1
                || maxCapacity > stack.Count)
            {
                // Есть место в пуле
                stack.Push(item);
            }
            else
            {
                GameObject.Destroy(item.gameObject);
            }
        }

        #endregion
    }
}
