﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;

namespace SpaceSim.Util
{
    /// <summary>
    /// Простой интерфейс для безразмерного пула объектов.
    /// </summary>
    /// <typeparam name="T"></typeparam>
    public interface IPool<T>
    {
        /// <summary>
        /// Получить объект из пула
        /// </summary>
        /// <returns></returns>
        T Aquire();

        /// <summary>
        /// Вернуть объект в пул
        /// </summary>
        /// <param name="item"></param>
        void Release(T item);
    }
}
