﻿using UnityEngine;
using System.Collections;
using System;
using System.Linq;

namespace SpaceSim.UserInput
{

    /// <summary>
    /// Реализация IInputSource для ИИ.
    /// 
    /// Если игрок подходит близко - пытается найти огневое решение
    /// Если есть огневое решение - стреляет
    /// </summary>
    public class EnemyInputSource : MonoBehaviour, IInputSource
    {

        #region Константы
        /// <summary>
        /// Максимальный угол доворота на игрока по одной из осей, при котором доворот не производится (мёртвая зона)
        /// 
        /// Измеряется в радианах.
        /// Необходим для стабилизации корабля в области огневого решения
        /// </summary>
        private const float EPSILON = 0.01f;

        /// <summary>
        /// Максимальный угол доворота на игрока при котором открывается огонь.
        /// 
        /// Измеряется в радианах.
        /// </summary>
        private const float FIRE_SOLUTION_ANGLE = 0.5f;

        /// <summary>
        /// Максимальный угол доворота на игрока при котором включается тяга
        /// 
        /// Измеряется в радианах.
        /// </summary>
        private const float THRUST_ANGLE = 1f;

        /// <summary>
        /// Минимальная дистанция до игрока при которой включается тяга
        /// </summary>
        private const float MIN_THRUST_DISTANCE = 10;

        private const float MIN_THRUST_DISTANCE_SQUARED = MIN_THRUST_DISTANCE * MIN_THRUST_DISTANCE;

        #endregion

        #region Свойства IInputSource

        public float ThrustAxisInput { get; private set; }

        public float PitchAxisInput { get; private set; }

        public float RollAxisInput { get; private set; }

        public float YawAxisInput { get; private set; }

        public bool TriggerPressed { get; private set; }

        #endregion

        /// <summary>
        /// Положение цели (или null если цели нет)
        /// </summary>
        private Transform targetTransform;

        #region События Unity

        public void OnTriggerExit(Collider playerCollider)
        {
            // Предполагает что враг только один
            targetTransform = null;
        }

        public void OnTriggerEnter(Collider playerCollider)
        {
            // Предполагает что враг только один
            targetTransform = GetRootTransform(playerCollider);
            // Идёт противостояние с игроком - нужно довернуть на него и/или открыть огонь
            var playerTransform = playerCollider.transform;

            // вектор к игроку в локальных координатах
            Vector3 localDelta = transform.InverseTransformPoint(playerTransform.position);

            PitchAxisInput = CalculatePitchAxis(localDelta);
            YawAxisInput = CalculateYawAxis(localDelta);
            RollAxisInput = CalculateRollAxis(localDelta);

            ThrustAxisInput = CalculateThrustAxis(localDelta);

            TriggerPressed = CalculateTriggerPressed(localDelta);
        }

        public void FixedUpdate()
        {
            if (!targetTransform)
            {
                // Нет цели, либо цель уничтожена - сбрасываем ввод по осям
                ResetInput();
                return;
            }

            // Идёт противостояние с игроком - нужно довернуть на него и/или открыть огонь

            // вектор к игроку в локальных координатах
            Vector3 localDelta = transform.InverseTransformPoint(targetTransform.position);

            PitchAxisInput = CalculatePitchAxis(localDelta);
            YawAxisInput = CalculateYawAxis(localDelta);
            RollAxisInput = CalculateRollAxis(localDelta);

            ThrustAxisInput = CalculateThrustAxis(localDelta);

            TriggerPressed = CalculateTriggerPressed(localDelta);
        }

        #endregion

        #region Методы для рассчёта ИИ

        /// <summary>
        /// Проверяет, нужно ли нажать на гашетку
        /// </summary>
        /// <param name="localDelta">вектор к игроку в локальных координатах</param>
        private static bool CalculateTriggerPressed(Vector3 localDelta)
        {
            // Угол между вектором на игрока и локальной осью z
            float angle = Mathf.Deg2Rad * Vector3.Angle(Vector3.forward, localDelta);
            return angle <= FIRE_SOLUTION_ANGLE;
        }

        /// <summary>
        /// Рассчитывает новые данные для оси рыскания
        /// </summary>
        /// <param name="localDelta">вектор к игроку в локальных координатах</param>
        private static float CalculateYawAxis(Vector3 localDelta)
        {
            float angleYaw = Mathf.Atan2(localDelta.x, localDelta.z);
            if (localDelta.z > 0 && Mathf.Abs(angleYaw) <= EPSILON)
            {
                // в мёртвой зоне
                return 0;
            }
            else
            {
                // Максимально быстро поворачиваем к игроку
                return angleYaw > 0 ? 1 : -1;
            }
        }

        /// <summary>
        /// Рассчитывает новые данные для оси тангажа
        /// </summary>
        /// <param name="localDelta">вектор к игроку в локальных координатах</param>
        private static float CalculatePitchAxis(Vector3 localDelta)
        {
            float anglePitch = Mathf.Atan2(-localDelta.y, localDelta.z);
            if (localDelta.z > 0 && Mathf.Abs(anglePitch) <= EPSILON)
            {
                // в мёртвой зоне
                return 0;
            }
            else
            {
                // Максимально быстро поворачиваем к игроку
                return anglePitch > 0 ? 1 : -1;
            }
        }

        /// <summary>
        /// Рассчитывает новые данные для оси крена
        /// </summary>
        /// <param name="localDelta">вектор к игроку в локальных координатах</param>
        private static float CalculateRollAxis(Vector3 localDelta)
        {
            float angleRoll = Mathf.Atan2(localDelta.x, localDelta.y);
            if (localDelta.z > 0 && Mathf.Abs(angleRoll) <= EPSILON)
            {
                // в мёртвой зоне
                return 0;
            }
            else
            {
                // Максимально быстро поворачиваем к игроку
                return angleRoll > 0 ? 1 : -1;
            }
        }

        /// <summary>
        /// Рассчитывает новые данные для оси газа
        /// </summary>
        /// <param name="localDelta">вектор к игроку в локальных координатах</param>
        private static float CalculateThrustAxis(Vector3 localDelta)
        {
            if (localDelta.sqrMagnitude < MIN_THRUST_DISTANCE_SQUARED)
            {
                // Достаточно близко к игроку, тяга не нужна.
                return 0;
            }

            // Угол между вектором на игрока и локальной осью z
            float angle = Mathf.Deg2Rad * Vector3.Angle(Vector3.forward, localDelta);
            if (angle <= THRUST_ANGLE)
            {
                // Смотрим в правильном направлении
                return 1;
            }
            else
            {
                // не нужно ускоряться в этом направлении
                return 0;
            }
        }

        #endregion

        #region Вспомогательные методы

        /// <summary>
        /// Выставляет все оси в 0, прекращает огонь.
        /// </summary>
        private void ResetInput()
        {
            PitchAxisInput = 0;
            YawAxisInput = 0;
            RollAxisInput = 0;
            TriggerPressed = false;
        }

        /// <summary>
        /// Получение ссылки на ближайшего родителя с Rigidbody
        /// </summary>
        /// <param name="component"></param>
        /// <returns>Transform родителя, либо null если нет родителя с Rigidbody</returns>
        private static Transform GetRootTransform(Component component)
        {
            // Ближайший родитель с Rigidbody
            Rigidbody rigidbody = component.GetComponentInParent<Rigidbody>();

            return rigidbody ? rigidbody.transform : null;
        }

        #endregion
    }
}