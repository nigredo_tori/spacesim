﻿using UnityEngine;
using System.Collections;
using System;

namespace SpaceSim.UserInput
{

    /// <summary>
    /// Реализация IInputSource с использованием пользовательского ввода.
    /// </summary>
    /// <remarks>
    /// Управление отключается при получении сообщения OnSpaceSimGameEnded (через BroadcastMessage).
    /// Это сделано для того чтобы не перехватывать события движения и нажатия мыши после
    /// окончании игры (в меню окончания игры).
    /// </remarks>
    public class PlayerInputSource : MonoBehaviour, IInputSource
    {

        /// <summary>
        /// Чуствительность оси газа (для реализации инкрементальной оси)
        /// </summary>
        private const float THRUST_SENSITIVITY = 1;

        /// <summary>
        /// Закончилась ли игра?
        /// </summary>
        private bool gameEnded;

        #region Реализация IInputSource

        public float ThrustAxisInput { get; private set; }

        public float PitchAxisInput
        {
            get
            {
                // После окончания игры все оси возвращают 0
                if (gameEnded)
                {
                    return 0;
                }

                return Input.GetAxis("Pitch");
            }
        }

        public float RollAxisInput
        {
            get
            {
                // После окончания игры все оси возвращают 0
                if (gameEnded)
                {
                    return 0;
                }

                return Input.GetAxis("Roll");
            }
        }

        public float YawAxisInput
        {
            get
            {
                // После окончания игры все оси возвращают 0
                if (gameEnded)
                {
                    return 0;
                }

                return Input.GetAxis("Yaw");
            }
        }

        public bool TriggerPressed
        {
            get
            {
                // После окончания игры все оси возвращают 0
                if (gameEnded)
                {
                    return false;
                }

                return Input.GetAxis("Fire") > 0;
            }
        }

        #endregion

        #region Обработка событий Unity

        public void Awake()
        {
            gameEnded = false;
        }

        public void FixedUpdate()
        {
            // После окончания игры все оси возвращают 0
            if (gameEnded)
            {
                ThrustAxisInput = 0;
                return;
            }

            // Так как ось инкрементальная
            float thrustUserAxis = UnityEngine.Input.GetAxis("Thrust");
            float thrust = ThrustAxisInput;
            thrust += THRUST_SENSITIVITY * thrustUserAxis * Time.fixedDeltaTime;
            thrust = Mathf.Clamp(thrust, -1, 1);

            ThrustAxisInput = thrust;
        }

        #endregion

        #region Обработка глобальных событий игры

        /// <summary>
        /// Посылается при завершении игры
        /// </summary>
        public void OnSpaceSimGameEnded()
        {
            gameEnded = true;
        }
        #endregion
    }
}