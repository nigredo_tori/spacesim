﻿using UnityEngine;
using System.Collections;

namespace SpaceSim.UserInput
{
    /// <summary>
    /// Источник данных управления корабля.
    ///  Это может быть пользовательский ввод,
    /// либо данные от ИИ.
    /// </summary>
    public interface IInputSource
    {
        /// <summary>
        /// Ось газа. Предоставляемые значения - в промежутке [-1, 1]
        /// 0 - нейтральная позиция
        /// </summary>
        float ThrustAxisInput { get; }

        /// <summary>
        /// Ось крена. Предоставляемые значения - в промежутке [-1, 1]
        /// 0 - нейтральная позиция
        /// </summary>
        float RollAxisInput { get; }

        /// <summary>
        /// Ось рыскания. Предоставляемые значения - в промежутке [-1, 1]
        /// 0 - нейтральная позиция
        /// </summary>
        float YawAxisInput { get; }

        /// <summary>
        /// Ось тангажа. Предоставляемые значения - в промежутке [-1, 1]
        /// 0 - нейтральная позиция
        /// </summary>
        float PitchAxisInput { get; }

        /// <summary>
        /// Нажата ли гашетка
        /// </summary>
        bool TriggerPressed { get; }
    }
}
